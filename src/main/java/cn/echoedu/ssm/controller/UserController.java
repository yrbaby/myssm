package cn.echoedu.ssm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.echoedu.ssm.po.User;
import cn.echoedu.ssm.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/toadd",method=RequestMethod.POST)
	public String toadd(HttpServletRequest req){
		return "add";
	}
	
	/*请求格式：
	 * 
	 * {
		"id":17,
		"username":"123",
		"sex":"男",
		"birthday":"2017-05-06",
		"address":"123"
		}
	*/
	@RequestMapping(value="/save",method=RequestMethod.POST)
	//ajax请求的是json数据     必须进行json设置
    @ResponseBody
	public String save(@RequestBody User user){
		int res=userService.insert(user);
		System.out.println("save的返回值："+res);
		if(res==1){
			return "redirect:list";
		}
		return "";
	}

	
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public String delete(@RequestParam Integer id){
		System.out.println("//////"+id);
		
		int res=userService.deleteByPrimaryKey(id);
		System.out.println("删除的返回值："+res);
		if(res==1){
			return "redirect:list";
		}
			return "";
			
	}

	@RequestMapping(value="/toupdate",method=RequestMethod.GET)
	public String toupdate(@RequestParam Integer id,Model model){
		User user=userService.selectByPrimaryKey(id);
		model.addAttribute("user",user);
		return "update";		
	}
	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(@ModelAttribute User user){
		System.out.println("用户修改："+user.getUsername()+user.getBirthday());
		int res=userService.updateByPrimaryKey(user);
		System.out.println("修改的返回值："+res);
	    if(res==1){
	    	return "redirect:list";	
	    }
		return "";
	}
	
	@RequestMapping(value="/list")
	public String list(Model model){
		List<User> list=userService.selectAll();
		//model用于把值绑定到另一个 界面
		model.addAttribute("list",list);
		return "listUser";
	}
	
	
	
	
	
	
	
	
  
}
