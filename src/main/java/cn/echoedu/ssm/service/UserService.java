package cn.echoedu.ssm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.echoedu.ssm.mapper.UserMapper;
import cn.echoedu.ssm.po.User;
@Service
public class UserService {
   @Autowired
   UserMapper userMapper;
   
 
 public int insert(User record){
	 return userMapper.insert(record);	
 }
 
 public int deleteByPrimaryKey(Integer id){
	return userMapper.deleteByPrimaryKey(id);
 }
 
 public int updateByPrimaryKey(User record){
	return userMapper.updateByPrimaryKey(record);
 }
 
 public User selectByPrimaryKey(Integer id){
	return userMapper.selectByPrimaryKey(id);
 }
 
 public List<User> selectAll(){
	 return userMapper.selectAll();
 }
	 
}
