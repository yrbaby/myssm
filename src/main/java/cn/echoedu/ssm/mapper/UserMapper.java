package cn.echoedu.ssm.mapper;


import java.util.List;

import org.springframework.stereotype.Repository;

import cn.echoedu.ssm.po.User;
//@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    User selectByPrimaryKey(Integer id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);
}