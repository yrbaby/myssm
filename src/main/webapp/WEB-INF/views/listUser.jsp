<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}/user"></c:set>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.2.1.min.js"></script>
</head>
<body>


		<table border=1 width=100% style="text-align:center">
			<tr>
			    <th>ID</th>
				<th>姓名</th>
				<th>性别</th>
				<th>生日</th>
				<th>地址</th>
				<th width="20%">操作</th>
			</tr>
		


			<c:forEach var="s" items="${list}">
			<tr>
				<td><input type="checkbox" name="check" value="${s.id}"></td>
				<td>${s.username}</td>
				<td>${s.sex}</td>
				<td>${s.birthday}</td>
				<td>${s.address}</td>
				<td><a href="javascript:edit(${s.id})">修改</a>&nbsp;&nbsp;&nbsp;<a
					href="javascript:remove(${s.id})">删除</a></td>
			</tr>
			</c:forEach>

		</table>
		<br>
		<br>
		<input type="button" value="添加用户" onclick="toadd()"/>
<script type="text/javascript">
function edit(id){
	window.open ("${ctx}/edit?id="+id, "修改用户", "top=300, left=600, height=500, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
}
function remove(id){
	window.open ("${ctx}/edit?id="+id, "修改用户", "top=300, left=600, height=500, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");	
}
function toadd(){
	window.open ("${ctx}/toadd", "添加用户", "top=300, left=600, height=500, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
}

</script>
</body>
</html>