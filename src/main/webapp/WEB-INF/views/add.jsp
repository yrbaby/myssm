<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}/StuMain"></c:set>         
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.2.1.min.js"></script>
<title>增加用户</title>
</head>
<body>
<form id="userForm">
     姓名：<input type="text" name="username"/><br/>
     年龄：<input type="text" name="sex"/><br/>
     性别：<input type="text" name="birthday"/><br/>
     爱好：<input type="text" name="address"/><br/>
     <input type="button" value="保存" id="save"> 
 </form>
</body>
<script type="text/javascript">
$(document).ready(function(){
	$("#save").click(function(){
		var params=$("#userForm").serializeArray();
		var _json={};
		for(var item in params ){
			//循环数据，将数据封装成json格式
			_json[params[item].name]=params[item].value;
		}
		
		$.ajax({
			type:"POST",
			url: "${pageContext.request.contextPath}/user/save",
			data:JSON.stringify(_json),
			dataType:'json',
			headers:{
				Accept:"application/json",
				"Content-Type":"application/json"
			},
			success:function(data){
				alert(data.result);	
			},
			error:function(){
				alert("请求失败");
			}
		});
	});
});
</script>
</html>