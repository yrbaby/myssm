package cn.echoedu.ssm.test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.echoedu.ssm.po.User;
import cn.echoedu.ssm.service.UserService;

public class UserServiceTest {

	@Test
	public void test() {
		@SuppressWarnings("resource")
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring/applicationContext.xml");
		UserService userService=(UserService)applicationContext.getBean("userService");
		List<User> list=userService.selectAll();
		for (User user : list) {
			System.out.println(user.getUsername());
		}
	}
	

}
